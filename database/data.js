// Generate a sequence of Foos in collection

print("Creating foos");
let elements = [];
for (let i = 0; i < 1000; ++i) {
  let foo = {
    "stringValue": "Foo" + i,
    "numberValue": i,
  }
  elements = [...elements, foo];
}
db.foo.insertMany(elements);