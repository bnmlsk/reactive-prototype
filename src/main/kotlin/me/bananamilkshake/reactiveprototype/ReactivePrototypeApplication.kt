package me.bananamilkshake.reactiveprototype

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class ReactivePrototypeApplication

fun main(args: Array<String>) {
    SpringApplication.run(ReactivePrototypeApplication::class.java, *args)
}