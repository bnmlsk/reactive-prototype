package me.bananamilkshake.reactiveprototype

import org.bson.types.ObjectId

data class Foo(var id: ObjectId?,
               var stringValue: String?,
               var numberValue: Long?) {
    constructor() : this(null, null, null)
}