package me.bananamilkshake.reactiveprototype.http

import me.bananamilkshake.reactiveprototype.Foo
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux

@RestController
class HttpConsumeController(webClientBuilder: WebClient.Builder) {

    companion object {
        val log: Logger = LoggerFactory.getLogger(HttpConsumeController::class.java)
    }

    private val webClient: WebClient = webClientBuilder.baseUrl("http://localhost:9000").build()

    @GetMapping("/consume")
    fun consume() {
        webClient.get()
                .uri("/produce")
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .retrieve()
                .bodyToFlux(Foo::class.java)
                .subscribe { foo: Foo? -> log.info("Received {}", foo) }
    }

    // consumes Foos and groups them in batches using .window() method
    @GetMapping("/consume/batch")
    fun consumeBatch() {
        webClient.get()
                .uri("/produce")
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .retrieve()
                .bodyToFlux(Foo::class.java)
                .window(5) // to perform batching of consumed records
                .subscribe { flux: Flux<Foo> -> flux.collectList().subscribe { foos: List<Foo> -> log.info("Batch received {}", foos) } }
    }

}