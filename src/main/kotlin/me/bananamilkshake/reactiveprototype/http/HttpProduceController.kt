package me.bananamilkshake.reactiveprototype.http

import me.bananamilkshake.reactiveprototype.Foo
import me.bananamilkshake.reactiveprototype.service.ProducerService
import org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@RestController
class HttpProduceController(private val producerService: ProducerService) {

    @GetMapping(path = ["/produce"], produces = [APPLICATION_STREAM_JSON_VALUE])
    fun getAll(): Flux<Foo> {
        return producerService.produce()
    }
}