package me.bananamilkshake.reactiveprototype.websocket

import me.bananamilkshake.reactiveprototype.Foo
import me.bananamilkshake.reactiveprototype.service.ProducerService
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.stereotype.Controller
import reactor.core.publisher.Flux

@Controller
class RSocketProduceController(private val producerService: ProducerService) {

    @MessageMapping("foo")
    fun produce(): Flux<Foo> {
        return producerService.produce()
    }
}