package me.bananamilkshake.reactiveprototype.websocket

import io.rsocket.core.RSocketConnector
import io.rsocket.frame.decoder.PayloadDecoder
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.messaging.rsocket.RSocketConnectorConfigurer
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.messaging.rsocket.RSocketStrategies
import reactor.core.publisher.Mono
import java.net.URI

@Configuration
open class RSocketClientConfiguration(@Value("\${spring.rsocket.server.port}") private val port : Int) {

    @Bean
    open fun rSocketRequester(rSocketStrategies: RSocketStrategies?): Mono<RSocketRequester> {
        val connectorConfigurer = RSocketConnectorConfigurer { connector: RSocketConnector ->
            connector
                    .dataMimeType(MediaType.APPLICATION_STREAM_JSON_VALUE)
                    .payloadDecoder(PayloadDecoder.ZERO_COPY)
        }
        return RSocketRequester.builder()
                .rsocketStrategies(rSocketStrategies)
                .rsocketConnector(connectorConfigurer)
                .connectWebSocket(uri)
    }

    private val uri: URI
        get() = URI.create(String.format("ws://localhost:%d", port))
}