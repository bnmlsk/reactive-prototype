package me.bananamilkshake.reactiveprototype.websocket

import me.bananamilkshake.reactiveprototype.Foo
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class RSocketConsumeController(private val rSocketRequesterMono: Mono<RSocketRequester>) {

    @GetMapping("/consume/websocket")
    fun consume() {

        // consumes data from producer limiting rate
        // producer does not populate more data until
        // further request from consumer

        rSocketRequesterMono.subscribe { rSocketRequester: RSocketRequester ->
            rSocketRequester
                    .route("foo")
                    .retrieveFlux(Foo::class.java)
                    .log("consumer-rsocket")
                    .limitRate(6)
                    .subscribe()
        }
    }
}