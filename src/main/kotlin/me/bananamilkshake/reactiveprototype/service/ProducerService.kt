package me.bananamilkshake.reactiveprototype.service

import com.mongodb.reactivestreams.client.MongoCollection
import me.bananamilkshake.reactiveprototype.Foo
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux

@Service
class ProducerService(private val fooCollection: MongoCollection<Foo>) {

    fun produce(): Flux<Foo> {
        return Flux
                .from(fooCollection.find().batchSize(5).limit(20))
                .log("producer")
    }
}