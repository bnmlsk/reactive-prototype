package me.bananamilkshake.reactiveprototype.service

import com.mongodb.MongoClientSettings
import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoCollection
import me.bananamilkshake.reactiveprototype.Foo
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.pojo.PojoCodecProvider
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class FooCollectionConfiguration(
        @Value("\${spring.data.mongodb.database}") private val databaseName: String) {

    @Bean
    open fun fooCollection(client: MongoClient): MongoCollection<Foo> {
        val pojoCodecRegistry = CodecRegistries.fromRegistries(
                MongoClientSettings.getDefaultCodecRegistry(),
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()))
        return client
                .getDatabase(databaseName)
                .getCollection("foo", Foo::class.java)
                .withCodecRegistry(pojoCodecRegistry)
    }
}