# Reactive prototype

Small snippet to check how does reactor works when trying to fetch
data by portions (limit rate).

## Implementation variants

### Using Spring WebFlux
Example in `me.bananamilkshake.reactiveprototype.http` package. 
Unfortunately could not manage to make producer to follow
rate limit set by consumer.

Consumer takes elements in batches using `.window()` method call.

To start consumption call
```
curl --location --request GET 'localhost:9000/consume'
```

### Using RSocket
Using websocket connection setup. Managed to limit elements production from consumer 
side thanks to recent changes in RSocket implementation to remove forced limit. 

To start consumption call
```
curl --location --request GET 'localhost:9000/consume/websocket'
```
Follow logs to note that producer starts new elements fetch after 
new request from consumer.

## Elements production

Using Mongo Reactive client to produce prepopulated elements. Nice feature of this
client -- it is possible to set a batch size of elements to be fetched in one go
instead of making multiple calls requesting data document by document. Enable
`org.mongodb.driver` logging to spot this behaviour.

Execute from project root to startup Mongo container with generated documents:
```
docker rm -f -v redux-test-mongo && docker run -p 27020:27017 \
    -v `pwd`/database:/docker-entrypoint-initdb.d \
    --env MONGO_INITDB_DATABASE=reactive-test \
    --name redux-test-mongo \
    -d mongo
```